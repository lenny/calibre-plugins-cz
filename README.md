# CalimeplPacz

Calibre metadata plugin Pack Czech

Repository for all great and quality Czech sources of metadata for Calibre

# Pro Čechy

Metadata pluginy pro české zdroje pro Calibre

P.S.: píšu znova plugin pro Databazeknih.cz, protože ten původní blbne a navíc neumí co by mohl, a píšu ho s jiným identifikátorem, protože pro plnou funkčnost je potřeba měnit styl generování id

# Pokud Vám chybí plugin pro některou stánku nebo Vám něco nefuguje pište do issues nebo na mail marduke@centrum.cz

# Práce na pluginech
V roce 2013 jsem sice nějaký plugin napsal, ale bylo to hodně narychlo a výsledek se mi po měsícíh používání moc nezamlouval.
U ostatních pluginů to také nebyla žádná sláva takže se rozhodl to všechno zahodit a psat pořádně a na zelené louce.
Každopádně chci, aby každý plugin stahoval co možná nejvíc dat a aby byl rozumně konfigurovatelný
Až budu se svojí prací spokojen tak ji pošlu ať se stane součástí Calibre

## Struktura
Každý adresář představuje jeden plugin
Je možno sputit jeho test přes run.bat
Jelikož píšu v Pydev tak mi při více souborech v pluginu hlásí prostředí neznámé třídy. To jsem vyřešil buildovacím scriptem. Takže NELZE vzít obsah vybraného adresáře a zabalit jako Calibre plugin. Musí se spustit build.py/buidl.bat podobně jak je to v run.bat a poté teplve zabalit. Pluginy také obsahují pomocnou třídu na výpisy stažených dat Devel. Ta se musí přidat také.

## Hotovo z minula:
Bookfan.eu - stanka mela dlouho vypadky nebo aspon problemy s obrazky, dnes je z ni inzerentni rozcestnik :-(((

## Plan:
- [x] Databazeknih.cz - v1.0.0
- [x] cbdb.cz - v1.0.2
- [x] onlineknihovna.cz - v1.0.0
- [x] legie.cz - v1.0.1
- [x] kdb.cz - v1.0.1
- [x] pitaval.cz - v1.0.0
- [x] baila.cz - v1.0.0
- [x] palmknihy - v1.0.0
- [x] knihi - v1.0.1

## Detailní funkčnost
### Vše
- titul
- autoři
- kategorie == tagy
- obalka
- serie vč. indexu
- nakladatel
- rok vydání
- hodnocení (1-5 hvězdiček)
- i každého pluginu je možno vybrat max_search, označuje kolik detailů knihy se má detailně zpracovat, pokud jich vůbec tolik najde, pokud jich bude víc seřadí je podle přibližné relevance (odpovídající jméno a autoři) a vezme pouze tolik kolik určí parametr
- pokud je více obálek vrací dle max_cover option

### Databazeknih
- cca 182 000 knih
- obálka 100x166 - 100x171
- rozlišení povídky od knihy, pokud je detekována povídka je přidán tag Povídka
- tagy serveru(neplést s kategoriemi, server to rozlišuje) se přidávají do tagů knihy ke kategoriím
- výpis povídek a tag Sbírka povídek pokud se jedná o povídkovou sbírku
- výpis knih ve kterých se kniha nachází pokud se jedná o povídku
- rok vydání a prvního vydání
- edice se přidává mezitagy

### Cbdb
- cca 91 500 knih
- více obálek o velikosti 141x210 a občas větší
- série index je nespolehlivý, je udáván pouze seřezený seznam knih v sérii

### Onlineknihovna
- cca 1 300 knih
- obálky 477x756 a větší

### Legie
- databáze knih Fantasy a Sci-Fi
- cca 14 500 knih a 21500 povídek
- více obálek, 150x230
- série včetně nezařazených knih
- informace o světě knihy, možno zapnout/vypnout a definovat prefix světa
- detailní výpis ocenění knihy

### Kdb
- cca 60 000 knih
- více obálek různé velikosti, jpg i gif

### Pitaval
- databáze detektivní, thrillerové a špionážní literatury
- cca 11 300 knih a 5600 povídek

### Baila
- cca 1 200 000 knih všech kategorií
- obálka 140-160x225
- mnoho tagů

### Knihi
- cca 48 000 knih
- obrázky 40x61 - ignorovány
